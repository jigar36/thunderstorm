//
//  DayCell.swift
//  thunderstorm
//
//  Created by Jigar Panchal on 4/23/18.
//  Copyright © 2018 4SGM. All rights reserved.
//

import UIKit

class DayCell: UICollectionViewCell {
  
  static let resuseIdentifier = "DayCell"
  
  let viewController: DayVC

  override init(frame: CGRect) {
    viewController = DayVC()
    super.init(frame: frame)
    setupViewController()
  }
  
  required init?(coder aDecoder: NSCoder) {
    viewController = DayVC()
    super.init(coder: aDecoder)
    setupViewController()
  }
  
  private func setupViewController(){
    viewController.view.backgroundColor = .blue
    contentView.addSubview(viewController.view)
    
    if let view = viewController.view{
      view.translatesAutoresizingMaskIntoConstraints = false
      
      view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0.0).isActive = true
      view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0.0).isActive = true
      view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0.0).isActive = true
      view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0.0).isActive = true
    }
  }
}
