//
//  NowCell.swift
//  thunderstorm
//
//  Created by Jigar Panchal on 4/23/18.
//  Copyright © 2018 4SGM. All rights reserved.
//

import UIKit

class NowCell: UICollectionViewCell {
  static let resuseIdentifier = "NowCell"
  
  let viewController: NowVC
  
  override init(frame: CGRect) {
    viewController = NowVC()
    super.init(frame: frame)
    setupViewController()
  }
  
  required init?(coder aDecoder: NSCoder) {
    viewController = NowVC()
    super.init(coder: aDecoder)
    setupViewController()
  }
  
  private func setupViewController(){
    viewController.view.backgroundColor = .green
    contentView.addSubview(viewController.view)
    
    if let view = viewController.view{
      view.translatesAutoresizingMaskIntoConstraints = false
      
      view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0.0).isActive = true
      view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0.0).isActive = true
      view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0.0).isActive = true
      view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0.0).isActive = true
    }
  }
}
