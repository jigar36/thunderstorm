//
//  RootViewController.swift
//  thunderstorm
//
//  Created by Jigar Panchal on 4/23/18.
//  Copyright © 2018 4SGM. All rights reserved.
//

import UIKit

private let dataManager = DataManager(baseUrl: API.AuthenticatedBaseUrl)

class RootViewController: UIViewController {
  
  enum RootViewType: Int{
    case now = 0
    case day
    case week
    
    static var count: Int{
      return RootViewType.week.rawValue + 1
    }
  }
  
  @IBOutlet var collectionView: UICollectionView!

  // MARK: - Properties
  
  fileprivate var aspectRatio: CGFloat {
    switch traitCollection.horizontalSizeClass {
    case .compact:
      return 1.0
    default:
      return 1.0 / 3.0
    }
  }
  
  fileprivate let minimumInteritemSpacingForSection: CGFloat = 0.0
  fileprivate let minimumLineSpacingForSection: CGFloat = 0.0
  fileprivate let insetForSection = UIEdgeInsets()
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
        setupView()
      
      dataManager.weatherDataforLocation(latitude: Defaults.Latitude, longitude: Defaults.Longitude) { (response, error) in
        print(response)
      }

    }
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    collectionView.collectionViewLayout.invalidateLayout()
  }
  
  private func setupView(){
    setupCollectionView()
  }

  private func setupCollectionView(){
    
    collectionView.register(NowCell.classForCoder(), forCellWithReuseIdentifier: NowCell.resuseIdentifier)
    collectionView.register(DayCell.classForCoder(), forCellWithReuseIdentifier: DayCell.resuseIdentifier)
    collectionView.register(WeekCell.classForCoder(), forCellWithReuseIdentifier: WeekCell.resuseIdentifier)
  }
  
}

extension RootViewController: UICollectionViewDataSource{
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return RootViewType.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    guard let type = RootViewType(rawValue: indexPath.item) else {
      fatalError()
    }
    
    switch type {
    case .day:
      // Dequeue Reusable Cell
      return collectionView.dequeueReusableCell(withReuseIdentifier: NowCell.resuseIdentifier, for: indexPath)
    case .now:
      // Dequeue Reusable Cell
      return collectionView.dequeueReusableCell(withReuseIdentifier: DayCell.resuseIdentifier, for: indexPath)
    case .week:
      // Dequeue Reusable Cell
      return collectionView.dequeueReusableCell(withReuseIdentifier: WeekCell.resuseIdentifier, for: indexPath)
    }
    
  }
  
}
extension RootViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let bounds = collectionView.bounds
    
    return CGSize(width: (bounds.width * aspectRatio), height: bounds.height)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return minimumInteritemSpacingForSection
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return minimumLineSpacingForSection
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return insetForSection
  }
  
}

