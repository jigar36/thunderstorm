//
//  DataManager.swift
//  thunderstorm
//
//  Created by Jigar Panchal on 4/23/18.
//  Copyright © 2018 4SGM. All rights reserved.
//

import Foundation

enum DataManagerError: Error {
  
  case Unknown
  case FailedRequest
  case InvalidResponse
  
}


final class DataManager {
  
  typealias WeatherDataCompletion = (AnyObject?, DataManagerError?) -> ()
  
  
  let baseUrl: URL
  init(baseUrl: URL) {
    self.baseUrl = baseUrl
  }
  
  func weatherDataforLocation(latitude: Double, longitude: Double, completion: @escaping WeatherDataCompletion){
    
    let URL = baseUrl.appendingPathComponent("\(latitude),\(longitude)")
    
    URLSession.shared.dataTask(with: URL, completionHandler: {(data, response, error) in
        self.didFetchWeatherData(data: data, response: response, error: error, completion: completion)
    }).resume()
    
  }
  
  private func didFetchWeatherData(data: Data?, response: URLResponse?, error: Error?, completion: WeatherDataCompletion) {
    if let _ = error{
      completion(nil, .FailedRequest)
    }else if let data = data, let response = response as? HTTPURLResponse{
      if response.statusCode == 200{
        processWeatherData(data: data, completion: completion)
      }else{
        completion(nil, .FailedRequest)
      }
    }else{
      completion(nil, .Unknown)
    }
  }

  private func processWeatherData(data: Data, completion: WeatherDataCompletion) {
    if let JSON = try? JSONSerialization.jsonObject(with: data, options: []) as AnyObject {
      completion(JSON, nil)
    } else {
      completion(nil, .InvalidResponse)
    }
  }
  
}
