//
//  Configuration.swift
//  thunderstorm
//
//  Created by Jigar Panchal on 4/23/18.
//  Copyright © 2018 4SGM. All rights reserved.
//

import Foundation

struct API {
  
  static let APIKEY = "32dede32b2016f2f47bee45b1cfc4cb1"
  static let BASE_URL = URL(string: "https://api.darksky.net/forecast/")!
  
  static var AuthenticatedBaseUrl: URL{
    return BASE_URL.appendingPathComponent(APIKEY)
  }
}

struct Defaults {
  
  static let Latitude: Double = 37.8267
  static let Longitude: Double = -122.423
  
}
