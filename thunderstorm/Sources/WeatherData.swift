//
//  WeatherData.swift
//  thunderstorm
//
//  Created by Jigar Panchal on 4/23/18.
//  Copyright © 2018 4SGM. All rights reserved.
//

import Foundation

struct WeatherData {
  
   let lat: Double
   let long: Double
  
   let hourData: [WeatherHourData]
  
  init(lat: Double, long: Double, hourData: [WeatherHourData]) {
    self.lat = lat
    self.long = long
    self.hourData = hourData
  }
  
}

extension WeatherData: JSONDecodable{

   init?(JSON: Any) {
    guard let JSON = JSON as? [String: AnyObject] else { return nil }
    
    guard let lat = JSON["latitude"] as? Double else { return nil }
    guard let long = JSON["longitude"] as? Double else { return nil }
    guard let hourlyData = JSON["hourly"]?["data"] as? [[String: AnyObject]] else { return nil }
    
    self.lat = lat
    self.long = long
    
    var buffer = [WeatherHourData]()
    
    for hourlyDataPoint in hourlyData {
      if let weatherHourData = WeatherHourData(JSON: hourlyDataPoint){
        buffer.append(weatherHourData)
      }
    }
    
    self.hourData = buffer
  }

}
