//
//  JSONDecodable.swift
//  thunderstorm
//
//  Created by Jigar Panchal on 5/29/18.
//  Copyright © 2018 4SGM. All rights reserved.
//

import Foundation

protocol JSONDecodable{
  
  init?(JSON: Any)
  
}
